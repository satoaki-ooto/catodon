# API Documentation

You can find interactive API documentation at any Catodon instance. https://catodon.social/api-doc

You can also find auto-generated documentation for iceshrimp-js [here](../packages/iceshrimp-js/markdown/iceshrimp-js.md).
