import define from "../define.js";
import config from "@/config/index.js";

export const meta = {
	tags: ["meta"],

	requireCredential: false,
	requireCredentialPrivateMode: true,
} as const;

export const paramDef = {
	type: "object",
	properties: {},
	required: [],
} as const;

export default define(meta, paramDef, async () => {
	let tag_name: string | null = null;
	try {
		await fetch(
			`https://codeberg.org/api/v1/repos/catodon/catodon/releases?draft=false&pre-release=${config.version.includes('-pre')}&page=1&limit=1`,
		)
			.then((response) => response.json())
			.then((data) => {
				if (data.length > 0) {
					tag_name = data[0].tag_name;
				}
			});
	} catch {
		tag_name = null
	}

	return {
		tag_name,
	};
});
