import { Brackets, SelectQueryBuilder, WhereExpressionBuilder } from "typeorm";
import { sqlLikeEscape } from "@/misc/sql-like-escape.js";
import {
	Followings,
	NoteFavorites,
	NoteReactions,
	Users,
} from "@/models/index.js";

const filters = {
	from: fromFilter,
	"-from": fromFilterInverse,
	mention: mentionFilter,
	"-mention": mentionFilterInverse,
	reply: replyFilter,
	"-reply": replyFilterInverse,
	to: replyFilter,
	"-to": replyFilterInverse,
	before: beforeFilter,
	until: beforeFilter,
	after: afterFilter,
	since: afterFilter,
	instance: instanceFilter,
	"-instance": instanceFilterInverse,
	domain: instanceFilter,
	"-domain": instanceFilterInverse,
	host: instanceFilter,
	"-host": instanceFilterInverse,
	filter: miscFilter,
	"-filter": miscFilterInverse,
	in: inFilter,
	"-in": inFilterInverse,
	has: attachmentFilter,
} as Record<
	string,
	(query: SelectQueryBuilder<any>, search: string, id: number) => any
>;

export function generateFtsQuery(
	query: SelectQueryBuilder<any>,
	q: string,
): void {
	const components = q.trim().split(" ");
	const terms: string[] = [];
	let counter = 0;

	for (const component of components) {
		const split = component.split(":");
		if (split.length > 1 && filters[split[0]] !== undefined)
			filters[split[0]](query, split.slice(1).join(":"), counter++);
		else terms.push(component);
	}

	appendSearchQuery(terms.join(" "), query, query);
}

function fromFilter(
	query: SelectQueryBuilder<any>,
	filter: string,
	id: number,
) {
	const userQuery = generateUserSubquery(filter, id);
	query.andWhere(`note.userId = (${userQuery.getQuery()})`);
	query.setParameters(userQuery.getParameters());
}

function fromFilterInverse(
	query: SelectQueryBuilder<any>,
	filter: string,
	id: number,
) {
	const userQuery = generateUserSubquery(filter, id);
	query.andWhere(`note.userId <> (${userQuery.getQuery()})`);
	query.setParameters(userQuery.getParameters());
}

function mentionFilter(
	query: SelectQueryBuilder<any>,
	filter: string,
	id: number,
) {
	const userQuery = generateUserSubquery(filter, id);
	query.addCommonTableExpression(userQuery.getQuery(), `cte_${id}`, {
		materialized: true,
	});
	query.andWhere(
		`note.mentions @> array[(SELECT * FROM cte_${id})]::varchar[]`,
	);
	query.setParameters(userQuery.getParameters());
}

function mentionFilterInverse(
	query: SelectQueryBuilder<any>,
	filter: string,
	id: number,
) {
	const userQuery = generateUserSubquery(filter, id);
	query.addCommonTableExpression(userQuery.getQuery(), `cte_${id}`, {
		materialized: true,
	});
	query.andWhere(
		`NOT (note.mentions @> array[(SELECT * FROM cte_${id})]::varchar[])`,
	);
	query.setParameters(userQuery.getParameters());
}

function replyFilter(
	query: SelectQueryBuilder<any>,
	filter: string,
	id: number,
) {
	const userQuery = generateUserSubquery(filter, id);
	query.andWhere(`note.replyUserId = (${userQuery.getQuery()})`);
	query.setParameters(userQuery.getParameters());
}

function replyFilterInverse(
	query: SelectQueryBuilder<any>,
	filter: string,
	id: number,
) {
	const userQuery = generateUserSubquery(filter, id);
	query.andWhere(`note.replyUserId <> (${userQuery.getQuery()})`);
	query.setParameters(userQuery.getParameters());
}

function beforeFilter(query: SelectQueryBuilder<any>, filter: string) {
	query.andWhere("note.createdAt < :before", { before: filter });
}

function afterFilter(query: SelectQueryBuilder<any>, filter: string) {
	query.andWhere("note.createdAt > :after", { after: filter });
}

function instanceFilter(
	query: SelectQueryBuilder<any>,
	filter: string,
	id: number,
) {
	if (filter === "local") {
		query.andWhere(`note.userHost IS NULL`);
	} else {
		query.andWhere(`note.userHost = :instance_${id}`);
		query.setParameter(`instance_${id}`, filter);
	}
}

function instanceFilterInverse(
	query: SelectQueryBuilder<any>,
	filter: string,
	id: number,
) {
	if (filter === "local") {
		query.andWhere(`note.userHost IS NOT NULL`);
	} else {
		query.andWhere(`note.userHost <> :instance_${id}`);
		query.setParameter(`instance_${id}`, filter);
	}
}

function miscFilter(query: SelectQueryBuilder<any>, filter: string) {
	let subQuery: SelectQueryBuilder<any> | null = null;
	if (filter === "followers") {
		subQuery = Followings.createQueryBuilder("following")
			.select("following.followerId")
			.where("following.followeeId = :meId");
	} else if (filter === "following") {
		subQuery = Followings.createQueryBuilder("following")
			.select("following.followeeId")
			.where("following.followerId = :meId");
	} else if (filter === "replies" || filter === "reply") {
		query.andWhere("note.replyId IS NOT NULL");
	} else if (
		filter === "boosts" ||
		filter === "boost" ||
		filter === "renotes" ||
		filter === "renote"
	) {
		query.andWhere("note.renoteId IS NOT NULL");
	}

	if (subQuery !== null)
		query.andWhere(`note.userId IN (${subQuery.getQuery()})`);
}

function miscFilterInverse(query: SelectQueryBuilder<any>, filter: string) {
	let subQuery: SelectQueryBuilder<any> | null = null;
	if (filter === "followers") {
		subQuery = Followings.createQueryBuilder("following")
			.select("following.followerId")
			.where("following.followeeId = :meId");
	} else if (filter === "following") {
		subQuery = Followings.createQueryBuilder("following")
			.select("following.followeeId")
			.where("following.followerId = :meId");
	} else if (filter === "replies" || filter === "reply") {
		query.andWhere("note.replyId IS NULL");
	} else if (
		filter === "boosts" ||
		filter === "boost" ||
		filter === "renotes" ||
		filter === "renote"
	) {
		query.andWhere("note.renoteId IS NULL");
	}

	if (subQuery !== null)
		query.andWhere(`note.userId NOT IN (${subQuery.getQuery()})`);
}

function inFilter(query: SelectQueryBuilder<any>, filter: string) {
	let subQuery: SelectQueryBuilder<any> | null = null;
	if (filter === "bookmarks") {
		subQuery = NoteFavorites.createQueryBuilder("bookmark")
			.select("bookmark.noteId")
			.where("bookmark.userId = :meId");
	} else if (
		filter === "favorites" ||
		filter === "favourites" ||
		filter === "reactions" ||
		filter === "likes"
	) {
		subQuery = NoteReactions.createQueryBuilder("react")
			.select("react.noteId")
			.where("react.userId = :meId");
	}

	if (subQuery !== null) query.andWhere(`note.id IN (${subQuery.getQuery()})`);
}

function inFilterInverse(query: SelectQueryBuilder<any>, filter: string) {
	let subQuery: SelectQueryBuilder<any> | null = null;
	if (filter === "bookmarks") {
		subQuery = NoteFavorites.createQueryBuilder("bookmark")
			.select("bookmark.noteId")
			.where("bookmark.userId = :meId");
	} else if (
		filter === "favorites" ||
		filter === "favourites" ||
		filter === "reactions" ||
		filter === "likes"
	) {
		subQuery = NoteReactions.createQueryBuilder("react")
			.select("react.noteId")
			.where("react.userId = :meId");
	}

	if (subQuery !== null)
		query.andWhere(`note.id NOT IN (${subQuery.getQuery()})`);
}

function attachmentFilter(query: SelectQueryBuilder<any>, filter: string) {
	switch (filter) {
		case "image":
			query.andWhere(`note."attachedFileTypes"::varchar ILIKE '%image/%'`);
			break;
		case "video":
			query.andWhere(`note."attachedFileTypes"::varchar ILIKE '%video/%'`);
			break;
		case "audio":
			query.andWhere(`note."attachedFileTypes"::varchar ILIKE '%audio/%'`);
			break;
		case "file":
			query.andWhere(`note."attachedFileTypes" <> '{}'`);
			query.andWhere(
				`NOT (note."attachedFileTypes"::varchar ILIKE '%image/%')`,
			);
			query.andWhere(
				`NOT (note."attachedFileTypes"::varchar ILIKE '%video/%')`,
			);
			query.andWhere(
				`NOT (note."attachedFileTypes"::varchar ILIKE '%audio/%')`,
			);
			break;
		default:
			break;
	}
}

function generateUserSubquery(filter: string, id: number) {
	if (filter.startsWith("@")) filter = filter.substring(1);
	const split = filter.split("@");

	const query = Users.createQueryBuilder("user")
		.select("user.id")
		.where(`user.usernameLower = :user_${id}`)
		.andWhere(
			`user.host ${split[1] !== undefined ? `= :host_${id}` : "IS NULL"}`,
		);

	query.setParameter(`user_${id}`, split[0].toLowerCase());

	if (split[1] !== undefined)
		query.setParameter(`host_${id}`, split[1].toLowerCase());

	return query;
}

function appendSearchQuery(
	term: string,
	query: SelectQueryBuilder<any>,
	qb: SelectQueryBuilder<any> | WhereExpressionBuilder,
) {
	qb.andWhere("note.text &@~ :q_term");
	query.setParameter(`q_term`, sqlLikeEscape(term));
}
