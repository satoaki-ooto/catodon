import { MigrationInterface, QueryRunner } from "typeorm";

export class RebrandCatodon1705287226019 implements MigrationInterface {
	async up(queryRunner: QueryRunner) {
		await queryRunner.query(
			`ALTER TABLE "meta" ALTER COLUMN "pinnedPages" SET DEFAULT '{/featured,/channels,/explore,/pages,/about-catodon}'`,
		);
		await queryRunner.query(
			`UPDATE meta SET "repositoryUrl" = 'https://codeberg.org/catodon/catodon'`,
		);
		await queryRunner.query(
			`UPDATE meta SET "feedbackUrl" = 'https://codeberg.org/catodon/catodon/issues'`,
		);
	}

	async down(queryRunner: QueryRunner) {
		await queryRunner.query(
			`UPDATE meta SET "repositoryUrl" = 'https://iceshrimp.dev/iceshrimp/iceshrimp'`,
		);
		await queryRunner.query(
			`UPDATE meta SET "feedbackUrl" = 'https://iceshrimp.dev/iceshrimp/iceshrimp/issues'`,
		);
		await queryRunner.query(
			`ALTER TABLE "meta" ALTER COLUMN "pinnedPages" SET DEFAULT '{/featured,/channels,/explore,/pages,/about-iceshrimp}'`,
		);
	}
}
