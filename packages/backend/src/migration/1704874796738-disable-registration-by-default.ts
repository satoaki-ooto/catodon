import { MigrationInterface, QueryRunner } from "typeorm";

export class DisableRegistrationByDefault1704874796738
	implements MigrationInterface
{
	async up(queryRunner: QueryRunner) {
		await queryRunner.query(
			`ALTER TABLE "meta" ALTER COLUMN "disableRegistration" SET DEFAULT true`,
		);
	}

	async down(queryRunner: QueryRunner) {
		await queryRunner.query(
			`ALTER TABLE "meta" ALTER COLUMN "disableRegistration" SET DEFAULT false`,
		);
	}
}
