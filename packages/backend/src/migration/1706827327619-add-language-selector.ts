import { MigrationInterface, QueryRunner } from "typeorm";

export class AddLanguageSelector1706827327619 implements MigrationInterface {
	async up(queryRunner: QueryRunner) {
		await queryRunner.query(
			`ALTER TABLE "note" ADD COLUMN IF NOT EXISTS "lang" character varying(10)`,
		);
	}

	async down(queryRunner: QueryRunner) {
		await queryRunner.query(`ALTER TABLE "note" DROP COLUMN IF EXISTS "lang"`);
	}
}
