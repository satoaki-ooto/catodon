import { MigrationInterface, QueryRunner } from "typeorm";

export class NoteTextFtsIdx1700331070890 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		console.log("Indexing full text search, please hang tight!");
		await queryRunner.query("CREATE EXTENSION IF NOT EXISTS pgroonga");
		await queryRunner.query(
			`CREATE INDEX IF NOT EXISTS "note_text_fts_idx" ON "note" USING pgroonga ("text")`,
		);
		await queryRunner.query(
			`CREATE INDEX IF NOT EXISTS "user_name_fts_idx" ON "user" USING pgroonga ("name" pgroonga_varchar_full_text_search_ops_v2)`,
		);
		await queryRunner.query(
			`CREATE INDEX IF NOT EXISTS "user_profile_description_fts_idx" ON "user_profile" USING pgroonga ("description" pgroonga_varchar_full_text_search_ops_v2)`,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query("DROP EXTENSION IF EXISTS pgroonga CASCADE");
	}
}
