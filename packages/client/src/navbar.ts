import { computed, reactive } from "vue";
import { $i } from "./account";
import { ui } from "@/config";
import { unisonReload } from "@/scripts/unison-reload";
import { openHelpMenu_ } from "@/scripts/helpMenu";

export const navbarItemDef = reactive({
	notifications: {
		title: "notifications",
		icon: "ph-bell ph-bold ph-lg",
		show: computed(() => $i != null),
		indicated: computed(() => $i?.hasUnreadNotification),
		to: "/my/notifications",
	},
	messaging: {
		title: "messaging",
		icon: "ph-chats-teardrop ph-bold ph-lg",
		show: computed(() => $i != null),
		indicated: computed(() => $i?.hasUnreadMessagingMessage),
		to: "/my/messaging",
	},
	drive: {
		title: "drive",
		icon: "ph-hard-drive ph-bold ph-lg",
		show: computed(() => $i != null),
		to: "/my/drive",
	},
	followRequests: {
		title: "followRequests",
		icon: "ph-hand-waving ph-bold ph-lg",
		show: computed(() => $i?.isLocked || $i?.hasPendingReceivedFollowRequest),
		indicated: computed(() => $i?.hasPendingReceivedFollowRequest),
		to: "/my/follow-requests",
	},
	explore: {
		title: "explore",
		icon: "ph-hash ph-bold ph-lg",
		to: "/explore",
	},
	announcements: {
		title: "announcements",
		icon: "ph-megaphone ph-bold ph-lg",
		indicated: computed(() => $i?.hasUnreadAnnouncement),
		to: "/announcements",
	},
	search: {
		title: "search",
		icon: "ph-magnifying-glass ph-bold ph-lg",
		to: "/search",
	},
	favorites: {
		title: "favorites",
		icon: "ph-bookmark-simple ph-bold ph-lg",
		show: computed(() => $i != null),
		to: "/my/favorites",
	},
	pages: {
		title: "pages",
		icon: "ph-article ph-bold ph-lg",
		to: "/pages",
	},
	channels: {
		title: "channel",
		icon: "ph-text-align-left ph-bold ph-lg",
		to: "/channels",
	},
	ui: {
		title: "switchUi",
		icon: "ph-layout ph-bold ph-lg",
		action: () => {
			if (ui === "default") {
				localStorage.setItem("ui", "deck");
				unisonReload();
			} else {
				localStorage.setItem("ui", "default");
				unisonReload();
			}
		},
	},
	help: {
		title: "help",
		icon: "ph-info ph-bold ph-lg",
		action: (ev) => {
			openHelpMenu(ev);
		},
	},
	reload: {
		title: "reload",
		icon: "ph-arrow-clockwise ph-bold ph-lg",
		action: () => {
			location.reload();
		}
	}
});

function openHelpMenu(ev: MouseEvent) {
	openHelpMenu_(ev);
}
