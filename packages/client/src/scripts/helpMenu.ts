import { instance } from "@/instance";
import { host } from "@/config";
import * as os from "@/os";
import { i18n } from "@/i18n";
import type { MenuItem } from "@/types/menu";

export function openHelpMenu_(ev: MouseEvent) {
	const menus: (MenuItem | null)[] = [
		{
			text: instance.name ?? host,
			type: "label",
		},
		{
			type: "link",
			text: i18n.ts.instanceInfo,
			icon: "ph-info ph-bold ph-lg",
			to: "/about",
		},
		{
			type: "link",
			text: i18n.ts.aboutIceshrimp,
			icon: "ph-lightbulb ph-bold ph-lg",
			to: "/about-catodon",
		},
	];
	if (instance.tosUrl) {
		menus.push({
			type: "button",
			text: i18n.ts.tos,
			icon: "ph-scroll ph-bold ph-lg",
			action: () => {
				window.open(instance.tosUrl, "_blank");
			},
		});
	}
	menus.push(null, {
		type: "parent",
		text: i18n.ts.developer,
		icon: "ph-code ph-bold ph-lg",
		children: [
			{
				type: "link",
				to: "/api-console",
				text: "API Console",
				icon: "ph-terminal-window ph-bold ph-lg",
			},
			{
				type: "button",
				text: i18n.ts.document,
				icon: "ph-file-doc ph-bold ph-lg",
				action: () => {
					window.open("/api-doc", "_blank");
				},
			},
			{
				type: "link",
				to: "/scratchpad",
				text: "AiScript Scratchpad",
				icon: "ph-scribble-loop ph-bold ph-lg",
			},
		],
	});
	const src = ev.currentTarget ?? ev.target ?? undefined;
	os.popupMenu(menus, src);
}
